<div id="dropZonePreview" style="display: none">
    <div class="dz-preview dz-file-preview">
        <div class="dz-details">
            <div class="dz-filename"><span data-dz-name></span></div>
            <div class="dz-size" data-dz-size></div>
            <input type="radio" class="static-selector" style="display: none;" onclick=" staticSelected(this)">
            <img data-dz-thumbnail />
            <div class="preview-holder">
                <input placeholder="Advertisement URL"  type="text" class="form-control imgUrl">
            </div>
        </div>
        <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>
        <%--<div class="dz-success-mark"><span>✔</span></div>
        <div class="dz-error-mark"><span>✘</span></div>--%>
        <div class="dz-error-message">
            <span data-dz-errormessage></span>
        </div>
    </div>
</div>