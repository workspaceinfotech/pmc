/**
 * Created by mi_rafi on 1/4/18.
 */

var errorFound = false;
var globalBtnAction = "";
var globalSubmitAction = "";

function SectionResource (token,url,selectedStatic) {
    this.token = token;
    this.url = url;
    this.selectedStatic = selectedStatic;
}
/*DropZoneObject used below code*/
var dropZoneElements = {
     advertiserOtherImages : {},

     galleryLogoImage : {},
     galleryBackgroundImage : {},
     galleryTopBanner : {},
     galleryBottomBanner : {},

     slideShowBannerOrVideo : {},
     slideShowVideo : {},

     popUpEmailBannerOrVideo : {},
     popUpSmsBannerOrVideo : {}
};


var PREFIX={
    SEC_RES:{
        URL:{
            _ADD:"imgUrl_",
            _UPDATE:"imgUrlUpdated_"
        },
        STATIC_SELECTOR:{
            _ADD:"staticSelectorAdd_",
            _UPDATE:"staticSelectorUpdate_"

        }

    }
};

var RotationSettings={
    _GALLERY_BOTTOM:"galleryBottomRotationBtn",
    _GALLERY_TOP:"galleryTopRotationBtn",
    _SLIDE_SHOW_BANNER:"slideShowBannerRotationBtn",
    _SLIDE_SHOW_VIDEO:"slideShowVideoRotationBtn",
    _POP_UP_SMS:"popUpSmsRotationBtn",
    _POP_UP_EMAIL:"popUpEmailRotationBtn"
};
function notifyUser(id,response,errorFound){
    if(errorFound){
        $("#"+id).html(response.responseJSON.length).show();
    }else{
        $("#"+id).html(0).hide();
    }
}
$(document).ready(function(){
    injectHiddenTokenFieldsForAdvertiser();

    for(var key in RotationSettings){
        var id = RotationSettings[key];
        initRotationSettings(id);
    }
    showHideLocationSelectBox();
    showHideEventSelectBox();
});

function getRotationSetting(id){
    var rotationSettingVal =  $("#"+id+" .active").data("val");
    rotationSettingVal = parseInt(rotationSettingVal);

    if(rotationSettingVal==1){
        return "ROTATE";
    }else{
        return "STATIC";
    }
}
function isRotationSettingsStatic(id,includeParent){
    var val =  (includeParent)?$("#"+id).parents(".imageupload").find(".btn-switch.active").first().attr("data-val"):
                                $("#"+id).find(".btn-switch.active").first().attr("data-val");
    try{
        val = parseInt(val);

    }catch(e){
        console.log(e);
    }
    if(val===0)
        return true;
    return false;
}
function initRotationSettings(id){
    $("#"+id+" .btn").click(function(){

        $("#"+id+" .btn").removeClass("active");
        $(this).addClass("active");

        $("#"+id+" .active").data("val");

        var rotationSettingVal =  $("#"+id+" .active").data("val");
        var parentElement = $("#"+id).parents(".imageupload").first();
        rotationSettingVal = parseInt(rotationSettingVal);
        if(rotationSettingVal===1){
            $(parentElement).find(".static-selector").hide();
        }else{
            $(parentElement).find(".static-selector").show();
        }

    });
}

var ADV_IMG_TYPE = {
    _ADVERTISER_OTHER_IMAGES_TOKEN:"otherImagesToken",
    _LOGO_TOKEN :"logoToken",
    _BACKGROUND_IMAGE:"backgroundImageToken",
    _TOP_BANNER_TOKEN:"topBannerToken",
    _BOTTOM_BANNER_TOKEN:"bottomBannerToken",
    _SLIDESHOW_BANNER_TOKEN:"slideShowBannerToken",
     _SLIDESHOW_VIDEO_TOKEN:"slideShowVideoToken",
    _EMAIL_POPUP_BANNER_TOKEN:"emailPopUpBannerToken",
    _EMAIL_POPUP_VIDEO_TOKEN:"emailPopUpVideoToken",
    _SMS_POPUP_BANNER_TOKEN:"smsPopUpBanner",
    _SMS_POPUP_VIDEO_TOKEN:"smsPopUpVideo"
};

function injectHiddenTokenFieldsForAdvertiser(){
    for(var index in ADV_IMG_TYPE){
        var tokenElem = $("<input>", {type:"hidden",id: ADV_IMG_TYPE[index], "value": ""});
        $("body").append(tokenElem);
    }
}


$(document).ready(function(){
    dropZoneElements.advertiserOtherImages  = configAdvertBdImgDropZone(
                                                {
                                                     elementId:"advertiserOtherImages",
                                                     param:"other-images",
                                                     maxFile:6,
                                                     maxFileSize:2,
                                                     preViewHtmlId:"dropZonePreviewWithOutUrl",
                                                     success:function(response){
                                                         storeToken(ADV_IMG_TYPE._ADVERTISER_OTHER_IMAGES_TOKEN,response.token);
                                                     },
                                                     afterServerFileRemove:function(response){
                                                         removeToken(ADV_IMG_TYPE._LOGO_TOKEN,response.token);
                                                     }
                                                });

    /*Gallery Ads*/
    dropZoneElements.galleryLogoImage = configAdvertBdImgDropZone(
                            {
                                elementId:"advLogo",
                                param:"logo-image",
                                maxFile:1,
                                maxFileSize:2,
                                success:function(response){
                                    emptyToken(ADV_IMG_TYPE._LOGO_TOKEN);
                                    storeToken(ADV_IMG_TYPE._LOGO_TOKEN,response.token);
                                },
                                afterServerFileRemove:function(response){
                                    removeToken(ADV_IMG_TYPE._LOGO_TOKEN,response.token);
                                }
                            });

    dropZoneElements.galleryBackgroundImage = configAdvertBdImgDropZone({
                                                        elementId:"advBackgroundImage",
                                                        param:"background-image",
                                                        maxFile:1,
                                                        maxFileSize:2,
                                                        success:function(response){
                                                            storeToken(ADV_IMG_TYPE._BACKGROUND_IMAGE,response.token);
                                                        },
                                                        afterServerFileRemove:function(response){
                                                            removeToken(ADV_IMG_TYPE._BACKGROUND_IMAGE,response.token);
                                                        }
                                                    });

    dropZoneElements.galleryTopBanner = configAdvertBdImgDropZone({
                                                            elementId:"advTopBannerImage",
                                                            param:"top-banner",
                                                            maxFile: 6,
                                                            maxFileSize: 1,
                                                            success:function(response){
                                                                storeToken(ADV_IMG_TYPE._TOP_BANNER_TOKEN,response.token);
                                                            },
                                                            afterServerFileRemove:function(response){
                                                                removeToken(ADV_IMG_TYPE._TOP_BANNER_TOKEN,response.token);
                                                            }
                                                        });

    dropZoneElements.galleryBottomBanner = configAdvertBdImgDropZone({
                                                        elementId:"advBottomBannerImage",
                                                        param:"bottom-banner",
                                                        maxFile: 6,
                                                        maxFileSize:2,
                                                        success:function(response){
                                                            storeToken(ADV_IMG_TYPE._BOTTOM_BANNER_TOKEN,response.token);
                                                        },
                                                        afterServerFileRemove:function(response){
                                                            removeToken(ADV_IMG_TYPE._BOTTOM_BANNER_TOKEN,response.token);
                                                        }
                                                    });
    /*Slide show Ads*/
    dropZoneElements.slideShowBannerOrVideo = configAdvertBdImgDropZone({
                                            elementId:"advSlideShowBanner",
                                            param:"slide-show-banner-or-video",
                                            maxFile:6,
                                            maxFileSize:50,
                                            success:function(response,file){
                                                if(isVideoFile(file)){
                                                    emptyToken(ADV_IMG_TYPE._SLIDESHOW_VIDEO_TOKEN);
                                                    storeToken(ADV_IMG_TYPE._SLIDESHOW_VIDEO_TOKEN,response.token);
                                                }else if(isImageFile(file)){
                                                    storeToken(ADV_IMG_TYPE._SLIDESHOW_BANNER_TOKEN,response.token);
                                                }
                                            },
                                            afterServerFileRemove:function(response){
                                                removeToken(ADV_IMG_TYPE._SLIDESHOW_BANNER_TOKEN,response.token);
                                                var key = "";
                                                if(isVideoFile(file)){
                                                    key = ADV_IMG_TYPE._SLIDESHOW_VIDEO_TOKEN;
                                                }else if(isImageFile(file)){
                                                    key = ADV_IMG_TYPE._SLIDESHOW_BANNER_TOKEN;
                                                }
                                                removeToken(key,response.token);
                                            },
                                            afterAddFile:function(elem){

                                                elem.on("maxfilesexceeded", function(file) {

                                                });
                                                elem.on("addedfile", function(file) {
                                                    /** Video or Image
                                                     * Image could be multiple
                                                     * Or One Video
                                                     * */

                                                    if(file.type.indexOf("video")>=0){
                                                        var files = dropZoneElements.slideShowBannerOrVideo.files;
                                                        for(var i=0;i<files.length-1;i++){
                                                            var tmpFile  = files[i];
                                                            tmpFile._removeLink.click();
                                                        }
                                                    }else{
                                                        var tmpFile = dropZoneElements.slideShowBannerOrVideo.files[0];
                                                        if(tmpFile!=undefined && tmpFile.type.indexOf("video")>=0){
                                                            tmpFile._removeLink.click();
                                                        }
                                                    }


                                                    file._removeLink.addEventListener("click", function() {
                                                        var key = "";
                                                        if(isVideoFile(file)){
                                                            key = ADV_IMG_TYPE._SLIDESHOW_VIDEO_TOKEN;
                                                        }else if(isImageFile(file)){
                                                            key = ADV_IMG_TYPE._SLIDESHOW_BANNER_TOKEN;
                                                        }

                                                        removeFileByToken(
                                                            file.token,
                                                            function(response){
                                                                removeToken(key,response.token);
                                                            }
                                                        );

                                                        elem.removeFile(file);
                                                    });
                                                });
                                            }
                                        });

    /*PopUp Ads*/
    dropZoneElements.popUpSmsBannerOrVideo = configAdvertBdImgDropZone({
        elementId:"advSmsPopUpBanner",
        param:"sms-popup-banner-or-video",
        maxFile:6,
        maxFileSize:50,
        success:function(response,file){
            if(isVideoFile(file)){
                emptyToken(ADV_IMG_TYPE._SMS_POPUP_VIDEO_TOKEN);
                storeToken(ADV_IMG_TYPE._SMS_POPUP_VIDEO_TOKEN,response.token);
            }else if(isImageFile(file)){
                storeToken(ADV_IMG_TYPE._SMS_POPUP_BANNER_TOKEN,response.token);
            }
        },
        afterServerFileRemove:function(response,file){
            var key = "";
            if(isVideoFile(file)){
                key = ADV_IMG_TYPE._SMS_POPUP_VIDEO_TOKEN;
            }else if(isImageFile(file)){
                key = ADV_IMG_TYPE._SMS_POPUP_BANNER_TOKEN;
            }
            removeToken(key,response.token);
        },
        afterAddFile:function(elem){

            elem.on("maxfilesexceeded", function(file) {

            });
            elem.on("addedfile", function(file) {
                /** Video or Image
                 * Image could be multiple
                 * Or One Video
                 * */

                file._removeLink.addEventListener("click", function() {
                    var key = "";
                    if(isVideoFile(file)){
                        key = ADV_IMG_TYPE._SMS_POPUP_VIDEO_TOKEN;
                    }else if(isImageFile(file)){
                        key = ADV_IMG_TYPE._SMS_POPUP_BANNER_TOKEN;
                    }
                    removeFileByToken(file.token,
                        function(response){
                            removeToken(key,response.token);
                        });

                    elem.removeFile(file);
                });




                /** Single Video file */
                if(isVideoFile(file)){
                    /** Remove all file before add */
                    var files = dropZoneElements.popUpSmsBannerOrVideo.files;
                    for(var i=0;i<files.length-1;i++){
                        var tmpFile  = files[i];
                        tmpFile._removeLink.click();
                    }
                }else{
                    /** Remove previously added video file */
                    var tmpFile = dropZoneElements.popUpSmsBannerOrVideo.files[0];
                    if(tmpFile!=undefined && tmpFile.type.indexOf("video")>=0){
                        tmpFile._removeLink.click();
                    }
                }

                if(this.files.length > this.options.maxFiles){
                    showServerErrorMessage("Maximum file limit is "+this.options.maxFiles,"tc",4000);
                    this.files[this.files.length-1]._removeLink.click();
                    this.removeFile(file);
                }

            });
        }
    });
    dropZoneElements.popUpEmailBannerOrVideo = configAdvertBdImgDropZone({
        elementId:"advEmailPopUpBanner",
        param:"email-popup-banner-or-video",
        maxFile:6,
        maxFileSize:50,
        success:function(response,file){
            if(isVideoFile(file)){
                emptyToken(ADV_IMG_TYPE._EMAIL_POPUP_VIDEO_TOKEN);
                storeToken(ADV_IMG_TYPE._EMAIL_POPUP_VIDEO_TOKEN,response.token);
            }else if(isImageFile(file)){
                storeToken(ADV_IMG_TYPE._EMAIL_POPUP_BANNER_TOKEN,response.token);
            }
        },
        afterServerFileRemove:function(response){
            var key = "";
            if(isVideoFile(file)){
                key = ADV_IMG_TYPE._SMS_POPUP_VIDEO_TOKEN;
            }else if(isImageFile(file)){
                key = ADV_IMG_TYPE._SMS_POPUP_BANNER_TOKEN;
            }
            removeToken(key,response.token);
        },
        afterAddFile:function(elem){

            elem.on("maxfilesexceeded", function(file) {

            });
            elem.on("addedfile", function(file) {
                /** Video or Image
                 * Image could be multiple
                 * Or One Video
                 * */

                if(file.type.indexOf("video")>=0){
                    var files = dropZoneElements.popUpEmailBannerOrVideo.files;
                    for(var i=0;i<files.length-1;i++){
                        var tmpFile  = files[i];
                        tmpFile._removeLink.click();
                    }
                }else{
                    var tmpFile = dropZoneElements.popUpEmailBannerOrVideo.files[0];
                    if(tmpFile!=undefined && tmpFile.type.indexOf("video")>=0){
                        tmpFile._removeLink.click();
                    }
                }


                file._removeLink.addEventListener("click", function() {
                    var key = "";
                    if(isVideoFile(file)){
                        key = ADV_IMG_TYPE._EMAIL_POPUP_VIDEO_TOKEN;
                    }else if(isImageFile(file)){
                        key = ADV_IMG_TYPE._EMAIL_POPUP_BANNER_TOKEN;
                    }

                    removeFileByToken(
                        file.token,
                        function(response){
                            removeToken(key,response.token);
                        }
                    );

                    elem.removeFile(file);
                });
            });
        }
    });

   



    /**Advertiser Info Date picker*/
    var runtimeStartsDate = ( $('#runtimeStarts').val().trim()=="" )?moment().format('MM/D/YYYY'):$('#runtimeStarts').val();
    var runtimeEndsStartDate = ( $('#runtimeEnds').val().trim()=="" )?moment().format('MM/D/YYYY'):$('#runtimeEnds').val();
    $('#runtimeStarts').daterangepicker({
        locale: {
            format: 'MM/DD/YYYY'
        },
        singleDatePicker: true,
        showDropdowns: true,
        startDate: runtimeStartsDate
    },function(start, end, label) {
        //var years = moment().diff(start, 'years');
        // alert("You are " + years + " years old.");
    });

    $('#runtimeEnds').daterangepicker({
        locale: {
            format: 'MM/DD/YYYY'
        },
        singleDatePicker: true,
        showDropdowns: true,
        startDate:runtimeEndsStartDate
    },function(start, end, label) {
        /*var years = moment().diff(start, 'years');
         alert("You are " + years + " years old.");*/
    });
    /**Gallery ads Date picker*/
    var topBannerExpiryDate = ( $('#topBannerExpiryDate').val().trim()=="" )?moment().format('MM/D/YYYY'):$('#topBannerExpiryDate').val();
    var bottomBannerExpiryDate = ( $('#bottomBannerExpiryDate').val().trim()=="" )?moment().format('MM/D/YYYY'):$('#bottomBannerExpiryDate').val();
    $('#topBannerExpiryDate').daterangepicker({
        locale: {
            format: 'MM/DD/YYYY'
        },
        singleDatePicker: true,
        showDropdowns: true,
        startDate:topBannerExpiryDate
    },function(start, end, label) {
        console.log(start, end, label);
        $("#topBannerExpiryDateLbl").html(start.format('MMM D, YYYY'));
        /*var years = moment().diff(start, 'years');
         alert("You are " + years + " years old.");*/
    });
    $('#bottomBannerExpiryDate').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        startDate:bottomBannerExpiryDate
    },function(start, end, label) {
        $("#bottomBannerExpiryDateLbl").html(start.format('MMM D, YYYY'));
        /*var years = moment().diff(start, 'years');
         alert("You are " + years + " years old.");*/
    });

    /*Slide show*/
    var slideShowBannerExpiryDate = ( $('#slideShowBannerExpiryDate').val().trim()=="" )?moment().format('MM/D/YYYY'):$('#slideShowBannerExpiryDate').val();

    $('#slideShowBannerExpiryDate').daterangepicker({
        locale: {
            format: 'MM/DD/YYYY'
        },
        singleDatePicker: true,
        showDropdowns: true,
        startDate:slideShowBannerExpiryDate
    },function(start, end, label) {
        $("#slideShowBannerExpiryDateLbl").html(start.format('MMM D, YYYY'));
        /*var years = moment().diff(start, 'years');
         alert("You are " + years + " years old.");*/
    });

    /*Popup ads*/
    var smsExpiryDate = ( $('#smsExpiryDate').val().trim()=="" )?moment().format('MM/D/YYYY'):$('#smsExpiryDate').val();
    var emailExpiryDate = ( $('#emailExpiryDate').val().trim()=="" )?moment().format('MM/D/YYYY'):$('#emailExpiryDate').val();

    $('#smsExpiryDate').daterangepicker({
        locale: {
            format: 'MM/DD/YYYY'
        },
        singleDatePicker: true,
        showDropdowns: true,
        startDate:smsExpiryDate
    },function(start, end, label) {
        $("#smsExpiryDateLbl").html(start.format('MMM D, YYYY'));
        /*var years = moment().diff(start, 'years');
         alert("You are " + years + " years old.");*/
    });
    $('#emailExpiryDate').daterangepicker({
        locale: {
            format: 'MM/DD/YYYY'
        },
        singleDatePicker: true,
        showDropdowns: true,
        startDate:emailExpiryDate
    },function(start, end, label) {
        $("#emailExpiryDateLbl").html(start.format('MMM D, YYYY'));
        /*var years = moment().diff(start, 'years');
         alert("You are " + years + " years old.");*/
    });

    $('input[name="starttime"]').timepicker({
        timeFormat: 'hh:mm tt'
    });

    $(".js-example-placeholder-multiple").select2({
        placeholder: "",
        allowClear: true
    });

    $("#allLocationSelection").click(function(){
        showHideLocationSelectBox();
    });
    $("#allEventSelection").click(function(){
        showHideEventSelectBox();
    });

    showHideLocationSelectBox();
    showHideEventSelectBox();

});
function showHideLocationSelectBox(){
    var isChecked = $("#allLocationSelection:checked").length;
    if(isChecked==1){
        $("#locationIds").parent().hide();
    }else{
        $("#locationIds").parent().show();
    }
}
function showHideEventSelectBox(){
    var isChecked = $("#allEventSelection:checked").length;
    if(isChecked==1){
        $("#eventIds").parent().hide();
    }else{
        $("#eventIds").parent().show();
    }
}
function configAdvertBdImgDropZone(data){
    var elementId = data.elementId;
    var param=data.param;
    var maxFile=data.maxFile;
    var maxFileSize=data.maxFileSize;
    var fnSuccess=data.success;
    var afterServerFileRemove=data.afterServerFileRemove;
    var addFileFn=data.afterAddFile;
    var preViewHtmlId = (data.preViewHtmlId ==undefined)?"dropZonePreview":data.preViewHtmlId;
    console.log(elementId);

    var advImgDropZone = new Dropzone("#"+elementId,
        {
            url: BASEURL+"file/upload/adv/"+param,
            method:"post",
            paramName:"advImg",
            maxFilesize: maxFileSize,
            maxFiles:maxFile,
            addRemoveLinks: true,
            previewTemplate:$("#"+preViewHtmlId).html(),
            previewsContainer: '#' + elementId + 'PreviewsContainer',
            init:function(){

                /** If function is overridden */
                if(addFileFn != undefined && typeof addFileFn == "function"){
                    addFileFn(this);
                }else{
                    /** Default behaviour */

                    this.on("maxfilesexceeded", function(file) {

                    });
                    this.on("addedfile", function(file) {
                        file._removeLink.addEventListener("click", function() {
                            console.log(file);
                            removeFileByToken(file.token,afterServerFileRemove,file);
                            advImgDropZone.removeFile(file);
                        });
                        if(this.files.length > maxFile){
                            if(maxFile==1){
                                console.log(this.files);
                                this.files[0]._removeLink.click();
                                console.log(this.files);
                            }else{
                                showServerErrorMessage("Maximum file limit is "+maxFile,"tc",4000);
                                this.files[advImgDropZone.files-1]._removeLink.click();
                                this.removeFile(file);
                            }
                        }


                    });
                }


            },
            error:function(file,response){
                console.log(file);
                var msg = (typeof response == "object")?((response.length>0)?response[0].msg:""):response;
                $(file.previewElement).find(".dz-error-message span").html(msg).addClass("text-danger");
            },
            success:function(file,response){
                console.log(file);
                /**
                 * Token Storing
                 * */
                file.token = response.token;
                fnSuccess(response,file);

                /**
                 * Show static selection radio
                 * */
                var isVideoFile = file.type.startsWith("video");
                var flag =  isRotationSettingsStatic(elementId,true);
                if(flag && !isVideoFile){
                    showStaticSectionRadio(elementId);
                }

                /**
                 * Assign id to Url text and Radio
                 * */
                $(file.previewElement).find(".imgUrl").first().attr("id",PREFIX.SEC_RES.URL._ADD+file.token);
                $(file.previewElement).find(".static-selector").first().attr("id",PREFIX.SEC_RES.STATIC_SELECTOR._ADD+file.token);

            }
        }
    );
    return advImgDropZone;
}

function initValidationForUpdate(btnAction,submitType){
    globalBtnAction = btnAction;
    UnBindErrors("errorObj_");
    errorFound = false;
    submitAdvertiserData(submitType,true,1);
}
function initSubmitAdvertiserData(btnAction,submitType){
    globalBtnAction = btnAction;
    UnBindErrors("errorObj_");
    errorFound = false;
    submitAdvertiserData(submitType,false,1);
}
function advertiserAfterSaveAction(btnAction,id){
    if(id==undefined){
        id=0;
    }
    var urlStr =BASEURL+"admin";
    switch(btnAction){
        case "save":
            urlStr += "/advertiser/update/"+id;
            break;
        case "save_and_close":
            urlStr += "/advertiser/all";
            break;
        case "save_and_new":
            urlStr += "/advertiser/add";
            break;
    }
    $.growl.notice({title: 'Success!', message: "Advertiser saved"});
    setTimeout(function(){
        window.location =urlStr;
    }, 600);

}
function submitAdvertiserData(submitType,forUpdate,marker){
    switch (marker){
        case 1:
            var tmpMarker = 2;
            validateAdvertiser(function(response){
                notifyUser("advertiserInfoErrorCount",response,false);
                submitAdvertiserData(submitType,forUpdate,tmpMarker);
            },function(response){
                BindErrorsWithHtml("errorObj_",response.responseJSON,true);
                notifyUser("advertiserInfoErrorCount",response,true);
                errorFound = true;
                submitAdvertiserData(submitType,forUpdate,tmpMarker);
            });
            break;
        case 2:
            validateGalleryAdds(forUpdate,function(response){
                notifyUser("galleryAdsErrorCount",response,false);
                submitAdvertiserData(submitType,forUpdate,3);
            },function(response){
                BindErrorsWithHtml("errorObj_",response.responseJSON,true);
                notifyUser("galleryAdsErrorCount",response,true);
                errorFound = true;
                submitAdvertiserData(submitType,forUpdate,3);
            });
            break;
        case 3:
            validateSlideShowAds(forUpdate,function(response){
                notifyUser("slideShowAdsErrorCount",response,false);
                submitAdvertiserData(submitType,forUpdate,4);
            },function(response){
                BindErrorsWithHtml("errorObj_",response.responseJSON,true);
                notifyUser("slideShowAdsErrorCount",response,true);
                errorFound = true;
                submitAdvertiserData(submitType,forUpdate,4);
            });
            break;
        case 4:
            validatePopUpAdsData(forUpdate,function(response){
                notifyUser("popUpAdsErrorCount",response,false);
                submitAdvertiserData(submitType,forUpdate,5);
            },function(response){
                BindErrorsWithHtml("errorObj_",response.responseJSON,true);
                notifyUser("popUpAdsErrorCount",response,true);
                errorFound = true;
                submitAdvertiserData(submitType,forUpdate,5);
            });
            break;
        case 5:
            if(!errorFound){
                submitCreateOrUpdate(submitType);
            }

            break;
    }
}
function submitCreateOrUpdate(btnAction){
       switch (btnAction){
           case "create":
               createAdvertiser(function(response){
                   notifyUser("advertiserInfoErrorCount",response,false);
                   advertiserAfterSaveAction(globalBtnAction,response.id);
               });
               break;
           case "update":
               updateAdvertiser();
               break;
       }
}
/*Advertiser */
function getAdvertiserInfoData(){
   
    var name = $('#name').val();
    var address = $('#address').val();
    var cityId = $('#cityId').val();
    var eventIds = [$('#eventIds').val()];
    var stateId = $('#stateId').val();
    var zip = $('#zip').val();
    var phone = $('#phone').val();
    var website = $('#website').val();
    var runtimeStarts = $('#runtimeStarts').data('daterangepicker').startDate._d.getTime();
    var runtimeEnds = $('#runtimeEnds').data('daterangepicker').startDate._d.getTime();
    var locationIds = [$('#locationIds').val()];
    var allLocationSelected = hasAllLocationSelected();
    var allEventSelected = hasAllEventSelected();
    var otherImage = getToken(ADV_IMG_TYPE._ADVERTISER_OTHER_IMAGES_TOKEN);

    var data={};

    data["name"]= name;
    data["address"]= address;
    data["cityId"]= cityId;
    data["eventIds"]= eventIds;
    data["stateId"]= stateId;
    data["zip"]= zip;
    data["phone"]= phone;
    data["website"]= website;
    data["otherImage"]= otherImage;
    data["runtimeStarts"]= runtimeStarts;
    data["runtimeEnds"]= runtimeEnds;
    data["locationIds"]= locationIds;
    data["isAllLocationSelected"]=allLocationSelected;
    data["isAllEventSelected"]=allEventSelected;
    data["otherImage"]=otherImage;


    return data;
}

/*Gallery Ads */
function getGalleryAddsData(){
    
    var galleryId = ($('#galleryAdId').length>0)?parseInt($('#galleryAdId').val()):0;
    var advertiserId = ($('#galleryAdId').length>0)?parseInt($('#advertiserId').val()):null;
    var logoToken = getToken(ADV_IMG_TYPE._LOGO_TOKEN);

    var bgImgTokens =  getToken(ADV_IMG_TYPE._BACKGROUND_IMAGE);
    var topBannerImgTokens = getToken(ADV_IMG_TYPE._TOP_BANNER_TOKEN);
    var bottomBannerImgTokens = getToken(ADV_IMG_TYPE._BOTTOM_BANNER_TOKEN);

    var topBannerExpiryDate = $('#topBannerExpiryDate').data('daterangepicker').startDate._d.getTime();
    var bottomBannerExpiryDate = $('#bottomBannerExpiryDate').data('daterangepicker').startDate._d.getTime();

    var topBannerRotation = getRotationSetting(RotationSettings._GALLERY_TOP);
    var bottomBannerRotation = getRotationSetting(RotationSettings._GALLERY_BOTTOM);

    var galleryAdBgPrice = $('#galleryAdBgPrice').val();
    var galleryAdTopBannerPrice = $('#galleryAdTopBannerPrice').val();
    var galleryAdBottomBannerPrice = $('#galleryAdBottomBannerPrice').val();

    var data={};
    if(galleryId>0)data["id"]= galleryId;

    data["advertiserId"]= advertiserId;
    data["logoSectionResource"]= getSectionResource(logoToken);
    data["bgSectionResource"]= getSectionResource(bgImgTokens);
    data["topSectionResource"]=getListOfSectionResource(topBannerImgTokens);
    data["bottomSectionResource"]=getListOfSectionResource(bottomBannerImgTokens);
    data["topBannerExpiryDate"]= topBannerExpiryDate;
    data["bottomBannerExpiryDate"]= bottomBannerExpiryDate;
    data["topBannerRotation"]= topBannerRotation;
    data["bottomBannerRotation"]= bottomBannerRotation;

    data["bgPrice"]=galleryAdBgPrice;
    data["topBannerPrice"] = galleryAdTopBannerPrice;
    data["bottomBannerPrice"] = galleryAdBottomBannerPrice;


    return data;
}
/*Slideshow Ads */
function getSlideShowAdsData(){
   

    var slideShowAdsId = ($("#slideshowAdId").length>0)?$("#slideshowAdId").val():0;
    var slideShowAdsBannerTokens = getToken(ADV_IMG_TYPE._SLIDESHOW_BANNER_TOKEN);
    var slideShowAdsVideoToken = getToken(ADV_IMG_TYPE._SLIDESHOW_VIDEO_TOKEN);
    var slideShowBannerDuration = $("#slideShowBannerDuration").val();
    var slideShowVideoDuration =  $("#slideShowVideoDuration").val();

    var bannerExpiryDate = $('#slideShowBannerExpiryDate').data('daterangepicker').startDate._d.getTime();

    var bannerRotation = getRotationSetting(RotationSettings._SLIDE_SHOW_BANNER);
    var videoRotation = getRotationSetting(RotationSettings._SLIDE_SHOW_VIDEO);

    var bannerPrice = $('#slideshowAdBannerPrice').val();
    var videoPrice = $('#slideshowAdVideoPrice').val();


    var data = {};
    if(slideShowAdsId>0) data["id"] = slideShowAdsId;


   // data[prefix+"slideShowAdsVideoToken"] = slideShowAdsVideoToken;


    data["slideShowAdsVideoResources"] =  getSectionResource(slideShowAdsVideoToken);
    data["slideShowAdsBannerResources"] = getListOfSectionResource(slideShowAdsBannerTokens);
    data["slideShowBannerDuration"] = slideShowBannerDuration;
    data["slideShowVideoDuration"] = slideShowVideoDuration;
    data["bannerExpiryDate"] = bannerExpiryDate  ;
    data["bannerRotation"] = bannerRotation;
    data["videoRotation"] = videoRotation;
    data["bannerPrice"] = bannerPrice;
    data["videoPrice"] = videoPrice;
    return data;
}
/*PopUp Ads */
function getPopUpAdsData(prefix){
    if(prefix==undefined){
        prefix="";
    }else{
        prefix += "."
    }


    var smsPopupId = $("#popupSmsAdId").length>0? parseInt($("#popupSmsAdId").val()):0;
    var smsPopupBanner = getToken(ADV_IMG_TYPE._SMS_POPUP_BANNER_TOKEN);
    var smsPopupVideo = getToken(ADV_IMG_TYPE._SMS_POPUP_VIDEO_TOKEN);
    var smsPopupVideoDuration = $("#smsPopupVideoDuration").val();
    var smsExpiryDate = $('#smsExpiryDate').data('daterangepicker').startDate._d.getTime();

    var emailPopupId = $("#popupEmailAdId").length>0? parseInt($("#popupEmailAdId").val()):0;
    var emailPopupVideo = getToken(ADV_IMG_TYPE._EMAIL_POPUP_VIDEO_TOKEN);
    var emailPopupBanner = getToken(ADV_IMG_TYPE._EMAIL_POPUP_BANNER_TOKEN);
    var emailPopupVideoDuration = $("#emailPopupVideoDuration").val();
    var emailExpiryDate =  $('#emailExpiryDate').data('daterangepicker').startDate._d.getTime();

    var smsRotation = getRotationSetting(RotationSettings._POP_UP_SMS);
    var emailRotation = getRotationSetting(RotationSettings._POP_UP_EMAIL);

    var smsAdPrice = $("#popUpAdSmsPrice").val();
    var emailAdPrice = $("#popUpAdEmailPrice").val();


    var data = {};
    if(smsPopupId>0)data[prefix+"smsId"]=smsPopupId;
    if(emailPopupId>0)data[prefix+"emailId"]=emailPopupId;

    data[prefix+"smsPopupBannerResources"]=getListOfSectionResource(smsPopupBanner);
    data[prefix+"smsPopupVideoResource"]=  getSectionResource(smsPopupVideo);
    data[prefix+"emailPopupBannerResources"]= getListOfSectionResource(emailPopupBanner);
    data[prefix+"emailPopupVideoResource"]= getSectionResource(emailPopupVideo);
    data[prefix+"emailPopupVideoDuration"]= emailPopupVideoDuration;
    data[prefix+"smsPopupVideoDuration"]= smsPopupVideoDuration;
    data[prefix+"smsExpiryDate"]= smsExpiryDate;
    data[prefix+"emailExpiryDate"]= emailExpiryDate;
    data[prefix+"smsRotation"]= smsRotation;
    data[prefix+"emailRotation"]= emailRotation;
    data[prefix+"smsAdPrice"]= smsAdPrice;
    data[prefix+"emailAdPrice"]= emailAdPrice;

    return data;
}
function getListOfSectionResource(tokens){
    var secResList = [];


   for(var i=0;i<tokens.length;i++){
       var token = tokens[i];
       var url = getSectionResourceUrlByToken(token);
       var isSelected = isStaticSelectorSelectedCheckByToken(token);
       secResList.push(new SectionResource(token,url,isSelected));
   }

   return secResList;
}
function getSectionResource(tokens){
    var sectionResource;
    if( tokens.length > 0){
        var url = getSectionResourceUrlByToken(tokens[0]);
        var isSelected = isStaticSelectorSelectedCheckByToken(tokens[0]);
        sectionResource = new SectionResource(tokens[0],url,isSelected);
    }else{
        sectionResource = new SectionResource(null,null,false);
    }
    return  sectionResource;
}
function getSectionResourceUrlByToken(token){
       var url =  $("#"+PREFIX.SEC_RES.URL._ADD+token).val();
       return ( url===undefined || url===null || url==="" )?null:url;
}
function isStaticSelectorSelectedCheckByToken(token){
    return  $("#"+PREFIX.SEC_RES.STATIC_SELECTOR._ADD+token).is(":checked");
}
function isStaticSelectorSelectedCheckBySecResId(id){
    return  $("#"+PREFIX.SEC_RES.STATIC_SELECTOR._UPDATE+id).is(":checked");
}
/*Validation */
function validateAdvertiser(fnSuccess,fnError){
    var data = getAdvertiserInfoData();
    console.log(data);
    $.ajax({
        url: BASEURL+"api/pmc-advsr/validate-create",
        type: "POST",
        dataType: "json",
        contentType: "application/json",
        data: JSON.stringify(data),
        statusCode: {
            500: function(response) {
                console.log(response);
            },
            401: function(response) {
                console.log(response.responseJSON);
            },
            422: function(response) {
                console.log(response.responseJSON);

                if(typeof fnError == "function"){
                    fnError(response)
                }

            }
        },
        success: function(response) {
            if(typeof fnSuccess == "function"){
                fnSuccess(response)
            }

        }
    });
}
function validateGalleryAdds(forUpdate,fnSuccess,fnError){
    var url = "";
    if(forUpdate==undefined || forUpdate==false){
        url = BASEURL+"api/pmc-adv/gallery-create-validation";
    }else{
        url = BASEURL+"api/pmc-adv/gallery-update-validation";
    }
    var data = getGalleryAddsData();
    $.ajax({
        url: url,
        type: "POST",
        dataType: "json",
        contentType: "application/json",
        data: JSON.stringify(data),
        statusCode: {
            500: function(response) {
                console.log(response);
            },
            401: function(response) {
                console.log(response.responseJSON);
            },
            422: function(response) {
                if(typeof fnError == "function"){
                    fnError(response);
                }
            }
        },
        success: function(response) {
            if(typeof fnSuccess == "function"){
                fnSuccess(response);
            }
        }
    });
}
function validateSlideShowAds(forUpdate,fnSuccess,fnError){
    var url="";
    if(forUpdate==undefined ||forUpdate==false){
        url = BASEURL+"api/pmc-adv/slideshow-create-validation";
    }else{
        url = BASEURL+"api/pmc-adv/slideshow-update-validation";
    }
    var data = getSlideShowAdsData();
    $.ajax({
        url: url,
        type: "POST",
        dataType: "json",
        contentType: "application/json",
        data: JSON.stringify(data),
        statusCode: {
            500: function(response) {
                console.log(response);
            },
            401: function(response) {
                console.log(response.responseJSON);
            },
            422: function(response) {
                if(typeof fnError == "function"){
                    fnError(response);
                }

            }
        },
        success: function(response) {
            console.log(response);
            if(typeof fnSuccess == "function"){
                fnSuccess(response);
            }

        }
    });
}
function validatePopUpAdsData(forUpdate,fnSuccess,fnError) {
    var url = "";
    if(forUpdate==undefined || forUpdate==false){
        url = BASEURL+"api/pmc-adv/popup-create-validation";
    }else{
        url = BASEURL+"api/pmc-adv/popup-update-validation";
    }
    var data = getPopUpAdsData();
    $.ajax({
        url: url,
        type: "POST",
        dataType: "json",
        contentType: "application/json",
        data: JSON.stringify(data),
        statusCode: {
            500: function(response) {
                console.log(response);
            },
            401: function(response) {
                console.log(response.responseJSON);
            },
            422: function(response) {
                if(typeof fnError == "function"){
                    fnError(response);
                }

            }
        },
        success: function(response) {
            if(typeof fnSuccess == "function"){
                fnSuccess(response);
            }

        }
    });
}

function hasAllLocationSelected(){
    var isChecked = $("#allLocationSelection:checked").length;
    return (isChecked==1)?true:false;
}
function hasAllEventSelected(){
    var isChecked = $("#allEventSelection:checked").length;
    return (isChecked==1)?true:false;
}
function staticSelected(elem){
    var parentElem = $(elem).parents(".imageupload").first();
    $(parentElem).find(".static-selector").each(function(){
        if(this!==elem){

            $(this).attr('checked', false);
        }
    });
}
function showStaticSectionRadio(elemId,includeParent){
    var parentElem = $("#"+elemId);
    if(includeParent){
        parentElem = $("#"+elemId).parents(".imageupload");
    }

    $(parentElem).find(".static-selector").each(function(){
        $(this).show();
    });
}
