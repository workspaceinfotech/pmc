package com.workspaceit.pmc.constant.watermark;

public class ImagePosition {
    private int x;
    private int y;

    public ImagePosition() {
    }

    public ImagePosition(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}
